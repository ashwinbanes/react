import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAUv3aKai2i8ncBUkd5CVgJqNgcjPvOtc8",
    authDomain: "react-5e90f.firebaseapp.com",
    databaseURL: "https://react-5e90f.firebaseio.com",
    projectId: "react-5e90f",
    storageBucket: "",
    messagingSenderId: "729081454939",
    appId: "1:729081454939:web:61b17817312932a2e93a7e"
}

export default !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();