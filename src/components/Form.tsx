import React from 'react';
import { reduxForm, Field } from 'redux-form';
import asyncValidate from '../utils/asyncValidate';
import validate from '../utils/validate'
import { Redirect, Link } from 'react-router-dom';
import { AppState } from '../state/AppState';

interface metaInterface {
  input: string,
  label: string,
  type: string,
  meta: { asyncValidating: string, touched: string, error: string }
}

const textField = {
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between'
}

const inputTextStyles = {
  border: 'none',
  fontFamily: 'Arial',
  fontSize: '14px',
  marginLeft: '5px',
  boxShadow: '0 6px 4px -4px rgba(0, 0, 0, .14)',
  marginBottom: '10px',
  height: '40px',
  width: '300px',
  outline: 'none'
}

const errorTextStyles = {
  color: 'red',
  fontSize: '12px',
  fontFamily: 'Arial'
}

const formStyles = {
  width: '320px',
  alignItems: 'center',
  justifyContent: 'space-between',
  height: '280px'
}

const buttonContainer = {
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-evenly',
  marginTop: '20px'
}

const buttonStyles = {
  width: '80px',
  height: '30px',
  color: 'white',
  backgroundColor: '#ff5063',
  border: 'none',
  fontSize: '12px',
  outline: 'none',
  cursor: 'pointer',
  boxShadow: '0 2px 2px 0 rgba(0, 0, 0, .14), 0 3px 1px -2px rgba(0, 0, 0, .2), 0 1px 5px 0 rgba(0, 0, 0, .12)',
}

// const renderField = (meta: metaInterface) => {
//   return (
//   <div style={textField}>
//     <div className={meta.meta.asyncValidating ? 'async-validating' : ''}>
//       <input style={inputTextStyles} {...meta.input}  type={meta.type} placeholder={meta.label} />
//       {
//         meta.meta.touched && meta.meta.error &&
//         <div style={errorTextStyles}>{meta.meta.error}</div>
//       }
//     </div>
//   </div>
//   )
// }


const renderField = (meta: metaInterface) => {
  return (
  <div style={textField}>
    <div>
      <input style={inputTextStyles} {...meta.input}  type={meta.type} placeholder={meta.label} />
      {
        meta.meta.touched && meta.meta.error &&
        <div style={errorTextStyles}>{meta.meta.error}</div>
      }
    </div>
  </div>
  )
}

const navigateToRedux = () => {
  // return <Redirect to="/redux" />
  // var store = AppState.getInstance().getStore()
  return <Link to="/redux" />
}


export const Form = (props: any) => {
  const { handleSubmit, pristine, reset, submitting } = props
    return (
      <div>
        <Link to="/redux">Navigate</Link>
        <form style={formStyles} onSubmit={ handleSubmit }>
              {/*<Field
                name="username"
                component={renderField}
                label="User Name"
              />
            <Field 
            name="firstName"
            component={renderField} 
            type="text"
            label="Firstname" />
            <Field 
            name="lastName" 
            component={renderField} 
            type="text"
              label="Lastname" />*/}
            <Field 
            name="email" 
            component={renderField} 
            type="email"
            label="Email" />
            <Field 
            name="password" 
            component={renderField} 
            type="password"
            label="Password" />
            <div style={buttonContainer}>
              <button style={buttonStyles} disabled={submitting} type="submit">
                SUBMIT
              </button>
              <button style={buttonStyles} disabled={ pristine || submitting } onClick={ reset }>
                CLEAR
              </button>
            </div>
        </form>
      </div>
  )
}

export default reduxForm({
  form: 'simple',
  validate,
  asyncBlurFields: ['username']
})(Form)
