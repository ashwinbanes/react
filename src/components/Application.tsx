import React, { Component } from 'react'
import { AppState } from '../state/AppState'

interface DummyProps {
    dummy: string
}

interface ApplicationState {
    value: string,
    counterValue: string,
    textInput: string,
    fruits: []
}

export class Application extends Component<DummyProps, ApplicationState> {

    constructor(props: DummyProps) {
        super(props);
        this.state = {
            value: '',
            counterValue: '0',
            textInput: '',
            fruits: []
        }
    }

    componentDidMount() {
        var types = AppState.getInstance().getTypes()
        var store = AppState.getInstance().getStore()
        this.setState({
            fruits: store.getState().orders.items
        });
        store.dispatch({
            "type": types.ADD,
            "items": 'Mangoes'
        })
        console.log(store.getState())
    }

    increment = () => {
        var types = AppState.getInstance().getTypes()
        var store = AppState.getInstance().getStore()
        store.dispatch({
            "type": types.INCREMENT
        });
        this.setState({
            counterValue: store.getState().counter.num
        });
    }

    decrement = () => {
        var types = AppState.getInstance().getTypes()
        var store = AppState.getInstance().getStore()
        store.dispatch({
            "type": types.DECREMENT
        });
        this.setState({
            counterValue: store.getState().counter.num
        });
    }

    getText = (text: string) => {
        this.setState({
            textInput: text
        });
    }

    addFruitsToList = () => {
        var types = AppState.getInstance().getTypes()
        var store = AppState.getInstance().getStore()
        store.dispatch({
            "type": types.ADD,
            "items": this.state.textInput
        });
        this.setState({
            fruits: store.getState().orders.items
        });
    }

    render() {
        return (
            <div>
                <p>Combined Reducers state management</p>
                <p>{ this.state.value }</p>
                <p>{ this.state.fruits }</p>
                <input onChange={ (text) => this.getText(text.target.value) } type="text" placeholder='Enter a name of a fruit...'/>
                <button onClick={ () => this.addFruitsToList() }>Add Fruit to list</button>
                <p>{ this.state.counterValue }</p>
                <button onClick={ () => this.increment() }>Hit to increment!</button>
                <button onClick={ () => this.decrement() }>Hit to decrement!</button>
            </div>
        )
    }
}

export default Application
