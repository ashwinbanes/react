import React, { Component } from 'react'
import ItemCard from './ItemCard'
import { AppState } from '../state/AppState'
import { Button } from 'reactstrap'
import '../component-styles/HomeStyles.css'
import { Link } from 'react-router-dom'

interface viewCartProps {

}

interface viewCartState {
    data: any[]
}

export class ViewCard extends Component<viewCartProps, viewCartState> {

    constructor(props: viewCartProps) {
        super(props)
        this.state = {
            data: []
        }
    }


    componentDidMount = () => {
        var types = AppState.getInstance().getTypes();
        var store = AppState.getInstance().getStore();

        store.dispatch({
            type: types.FETCH_CART_ITEMS
        })

        console.log('On Cart View Page')
        console.log(store.getState().cart.cartItems)

        this.setState({
            data: store.getState().cart.cartItems
        })

    }

    render() {
        return (
            <div className="homeContainer">
            {
                this.state.data.map((item: any, index: any) => {
                    return (
                        <ItemCard
                        items={item}
                        type='cart' />
                    )
                })
            }
            </div>
        )
    }
}

export default ViewCard
