import React, { Component } from 'react'
import ItemCard from './ItemCard'
import { AppState } from '../state/AppState'
import { Button } from 'reactstrap'
import '../component-styles/HomeStyles.css'
import { Link } from 'react-router-dom'

const homeContainer = {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: '20px'
}

interface homeProps {

}

interface homeState {
    data: any[],
    screen: string
}

export class Home extends Component<homeProps, homeState> {

    constructor(props: homeProps) {
        super(props)
        this.state = {
            data: [],
            screen: 'home'
        }
    }

    fetchData = () => {
        var store = AppState.getInstance().getStore()
        var types = AppState.getInstance().getTypes()
        store.dispatch({
            type: types.FETCH_HOME,
            payload: 'https://jsonplaceholder.typicode.com/posts'
        })
        this.setState({
            data: store.getState().populate.data
        })
    }

    clearData = () => {
        var store = AppState.getInstance().getStore()
        var types = AppState.getInstance().getTypes()
        store.dispatch({
            type: types.CLEAR_HOME
        })
        this.setState({
            data: []
        })
    }

    componentDidMount = () => {
        
    }

    render() {
        return (
            <div>
                <Button onClick={this.fetchData}>Fetch</Button>
                <br/>
                <Button onClick={this.clearData}>Clear</Button>
                <br/>
                <Link to="/cart">Go to cart</Link>
                <div className="homeContainer">
                {
                    this.state.data ? (
                        this.state.data.map((item: any, index: any) => {
                            return (
                                <ItemCard
                                items={item}
                                type='home' />
                            )
                        })
                    ) : (
                        <p>No items found try hitting the fetch button.</p>
                    )
                }
                </div>
            </div>
        )
    }
}

export default Home
