import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { Navbar, NavbarBrand } from 'reactstrap';

export const NavBar = () => {
    return (
        <Navbar color="light" expand="md">
        <NavbarBrand href="/">Redux</NavbarBrand>
        </Navbar>
    )
}

export default NavBar
