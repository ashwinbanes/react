import React, { Component } from 'react'
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';
import { AppState } from '../state/AppState';

const cardStyles = {
    width: '320px',
    height: 'auto',
    marginBottom: '20px'
}

const cardImgStyles = {
    height: '200px',
}

interface cardItems {
    cardTitle: string,
    cardSubTitle: string,
    cardText: string,
    buttonText: string,
    cardKey: string
}

interface cardProps {
    type: string,
    items: any
}

interface cardState {
    cardTitle: string,
    cardSubTitle: string,
    cardText: string,
    buttonText: string,
    cardKey: string,
    cartItems: string[],
    type: string
}

export class ItemCard extends Component<cardProps, cardState> {

    constructor(props: cardProps) {
        super(props)
        this.state = {
            cardTitle: this.props.items.title,
            cardSubTitle: this.props.items.title,
            cardText: this.props.items.title,
            buttonText: this.props.items.title,
            cardKey: this.props.items.title,
            type: this.props.type,
            cartItems: []
        }
    }

    passDataToParent = () => {
        var types = AppState.getInstance().getTypes()
        var store = AppState.getInstance().getStore()
        this.state.cartItems.push(this.state.cardKey)
        store.dispatch({
            type: types.ADD_ITEM_TO_CART,
            payload: this.state.cartItems
        })
        console.log('Passed to parent from : ' + this.state.cardKey)
        console.log(store.getState())
    }

    render() {
        console.log(this.props.items.cardTitle)
        return (
            <div>
                <Card style={cardStyles}>
                    <CardImg style={cardImgStyles} top width="100%" src="https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fs3.amazonaws.com%2Ffinecooking.s3.tauntonclud.com%2Fapp%2Fuploads%2F2017%2F04%2F18202120%2FOno-Japanese-Soul-Cooking-napolitan-spaghetti-recipe-main.jpg&f=1&nofb=1" alt="Spaghetti" />
                        <CardBody>
                            <CardTitle>{this.state.cardTitle}</CardTitle>
                            <CardSubtitle>{this.state.cardSubTitle}</CardSubtitle>
                            <CardText>
                            {this.state.cardText}
                            </CardText>
                            {
                                this.state.type === 'home' ? (
                                    <Button onClick={this.passDataToParent}>Add to cart</Button>
                                ) : (
                                    <Button onClick={this.passDataToParent}>Remove from cart</Button>
                                )
                            }
                            
                        </CardBody>
                </Card>
            </div>
        )
    }
}

export default ItemCard
