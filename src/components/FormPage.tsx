import React, { Component } from 'react'
import Form from '../components/Form';
import * as firebase from 'firebase';
import { AppState } from '../state/AppState';
import { Redirect, Link, Route } from 'react-router-dom';

const containerStyles = {
    width: '50%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '8px',
};

interface formResponse {
    email: string;
    firstName: string;
    lastName: string;
    username: string
}

export class FormPage extends Component {

    submit(values: any) {
        console.log(values)
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
            .then(() => {
                return firebase.auth().signInWithEmailAndPassword(values.email, values.password)
                .then(() => {
                    // alert('Logged in successfully')
                    // return <Redirect to="/redux" />
                    const appState = AppState.getInstance()
                    const types = appState.getTypes()
                    const store = appState.getStore()
                    store.dispatch({
                        "type": types.LOGIN,
                        "isAuth": true
                    })
                    console.log(store.getState().auth.isAuth)
                    if (store.getState().auth.isAuth) {
                        console.log('getting in')
                        return (
                            <Route
                            render={props => {
                                console.log('after logging in')
                                console.log(props)
                                return <Redirect
                                to={{
                                    pathname: "/redux",
                                    state: {
                                        from: props.location
                                    }
                                }}
                                />
                            }}
                            />
                        )
                    }
                })
            }).catch((error: any) => {
                alert('Login failed :( ' + error)
            })
    }

    render() {
        return (
            <div style={containerStyles}>
                <Form onSubmit={this.submit}></Form>
            </div>
        )
    }
}

export default FormPage
