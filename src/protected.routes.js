import React from "react";
import { Route, Redirect } from "react-router-dom";
import { AppState } from "./state/AppState";

export const ProtectedRoute = ({
  component: Component,
  ...rest
}) => {
    let appStore = AppState.getInstance().getStore()
    console.log('Inside protected routes :)')
    console.log(appStore.getState().auth.isAuth)
  return (
    <Route
      {...rest}
      render={props => {
        if (appStore.getState().auth.isAuth) {
            console.log('protected routes')
            console.log(props)
          return <Component {...props} />;
        } else {
            console.log('protected routes')
            console.log(props)
          return (
            <Redirect
              to={{
                pathname: "/login",
                state: {
                  from: props.location
                }
              }}
            />
          );
        }
      }}
    />
  );
};
