import types from '../actions/types'

const intialState = {
    cartItems: [] = []
}

function getUnique(array: any[]) {
    var unique = []

    for(let i = 0; i < array.length; i++) {
        if(unique.indexOf(array[i]) === -1) {
            unique.push(array[i]);
        }
    }

    return unique

}

export const cardReducer = (state = intialState, action: { type: any; payload: never; }) => {

    switch (action.type) {
        case types.ADD_ITEM_TO_CART:
            state.cartItems.push(action.payload)
            return state;
    
        case types.FETCH_CART_ITEMS:
            return state;

        default:
            return state;
    }
}

export default cardReducer