import types from '../actions/types'

const initialState = {
    items: []
}

interface action {
    type: string,
    items: []
}

export const fruitsReducer = (state = initialState, action: { type: any; items: never; }) => {

    switch (action.type) {
        case types.ADD:
            state.items.push(action.items);
            return state;

        case types.CLEAR:
            state.items = []
            return state

        case types.REMOVE:
            let index = state.items.indexOf(action.items)
            if(index > -1) {
                state.items.splice(index, 1)
            }
            return state

        default:
            return state;
    }

}

export default fruitsReducer