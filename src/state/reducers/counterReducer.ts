import { types } from '../actions/types';

export const counterReducer = (state = {
  num: 0
}, action: any) => {

  switch (action.type) {

    case types.INCREMENT:
      return {
        num: state.num + 1
      }

      case types.DECREMENT:
        return {
          num: state.num - 1
        }

        default:
          return state
  }


}

export default counterReducer