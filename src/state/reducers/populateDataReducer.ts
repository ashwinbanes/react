import types from '../actions/types'

const initialState = {
    data: []
}

interface action {
    type: string,
    payload: string
}

export const populateDataReducer = (state = initialState, act: action) => {
    switch (act.type) {
        case types.FETCH_HOME:
            fetch(act.payload)
            .then((response: any) => {
                return response.json()
            })
            .then((response: any) => {
                state.data = response
            })
            .catch(error => {
                console.log(error)
            })
            return state;
    
        case types.CLEAR_HOME:
            state.data = []

            return state

        default:
            return state;
    }
}

export default populateDataReducer