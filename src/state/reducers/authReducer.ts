import types from '../actions/types'

const initialState = {
    isAuth: false
}

interface action {
    type: string,
    isAuth: false
}

export const authReducer = (state = initialState, 
    act: action) => {

        switch (act.type) {
            case types.LOGIN:
                state.isAuth = act.isAuth
                return state
        
            case types.LOGOUT:
                state.isAuth = act.isAuth
                return state

            default:
                return state;
        }
}

export default authReducer