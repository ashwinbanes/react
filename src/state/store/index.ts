import {
    createStore, combineReducers
} from 'redux';
import { reducer as formReducer } from 'redux-form';
import { counterReducer } from '../reducers/counterReducer';
import { fruitsReducer } from '../reducers/fruitsReducer';
import { authReducer } from '../reducers/authReducer';
import populateDataReducer from '../reducers/populateDataReducer';
import cardReducer from '../reducers/cartReducer';

const reducer = combineReducers({
    orders: fruitsReducer,
    counter: counterReducer,
    form: formReducer,
    auth: authReducer,
    populate: populateDataReducer,
    cart: cardReducer
})

const store = createStore(reducer)

export default store;