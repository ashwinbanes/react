import types from './actions/types'
import store from './store/index';

let instance = null

export class AppState {

    constructor() {
        if (!instance) {
            instance = this
        }
        return instance
    }

    static getInstance() {
        if (instance == null) {
            instance = new AppState()
        }
        return instance
    }

    getStore() {
        return store
    }

    getTypes() {
        return types
    }

}

export default store