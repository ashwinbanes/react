import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';
import store from './state/store/index';
import * as firebase from 'firebase';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import Application from './components/Application';
import { ProtectedRoute } from './protected.routes';
import Home from './components/Home';
import NavBar from './components/NavBar';
import ViewCard from './components/ViewCard';

const firebaseConfig = {
    apiKey: "AIzaSyAUv3aKai2i8ncBUkd5CVgJqNgcjPvOtc8",
    authDomain: "react-5e90f.firebaseapp.com",
    databaseURL: "https://react-5e90f.firebaseio.com",
    projectId: "react-5e90f",
    storageBucket: "",
    messagingSenderId: "729081454939",
    appId: "1:729081454939:web:61b17817312932a2e93a7e"
}

firebase.initializeApp(firebaseConfig)

const routing = (
    <Provider store={store}>
    <Router>
        <NavBar />
        <Route path="/login" component={App} />
        <Route path="/home" component={Home} />
        <Route path="/cart" component={ViewCard} />
        <ProtectedRoute path="/redux" component={Application} />
    </Router>
    </Provider>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
