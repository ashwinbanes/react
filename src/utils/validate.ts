export const validate = (values: any) => {
    const errors = {
      username: '',
      password: '',
      firstName: '',
      lastName: '',
      email: ''
    } 
    if (!values.username) {
      errors.username = 'Fill up this field'
    }
    if (!values.password) {
      errors.password = 'Fill up this field'
    }
    if (!values.firstName) {
      errors.firstName = 'Fill up this field'
    }
    if (!values.lastName) {
      errors.lastName = 'Fill up this field'
    }
    if (!values.email) {
      errors.email = 'Fill up this field'
    }
    if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Enter a valid email address.'
    }
    return errors
  }

  export default validate