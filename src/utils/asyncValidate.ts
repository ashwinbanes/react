

const sleep = (ms: any) => new Promise(resolve => setTimeout(resolve, ms))

export const asyncValidate = (values: any) => {
    return sleep(1000).then(() => {
        if(['ashwin', 'banes', 'alex', 'alan'].includes(values.username.toLowerCase())) {
            throw { username: 'Username already exists' };
        }
    })
}

export default asyncValidate